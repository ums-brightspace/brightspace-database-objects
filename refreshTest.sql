
truncate table COLLEGE_COMPLETED;
truncate table COLLEGE_TRANSLATION;
truncate table COLLEGE_UNIT;
truncate table COURSE_COMPLETED;
truncate table ENROLLMENT_COMPLETED;
truncate table LOG;
truncate table SANDBOX_COMPLETED;
truncate table SEMESTER_COMPLETED;
truncate table UNIT_COMPLETED;
truncate table UNIT_TRANSLATION;
truncate table UNIVERSITY_COMPLETED;
truncate table USER_COMPLETED;

-- Copy from prod:  college_completed, college_translation, college_unit, semeseter_completed, unit_completed, unit_translation, university_completed
-- Let the integration recreate:  course_completed, enrollment_completed, user_completed
-- Leave log be so that we know what has happened in the test environment