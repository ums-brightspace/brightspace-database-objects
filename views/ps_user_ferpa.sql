Create or Replace Force View "PS_USER_FERPA" as
Select RU.roleuser emplid
From PS.CS_PSROLEUSER RU
Where RU.rolename in ('S_SS_FACULTY', 'S_SS_FACULTY_ADJUNCT')
Group By RU.roleuser;
