CREATE OR REPLACE FORCE VIEW ps_enroll_combined_instructor
AS
Select CC.institution
     , CC.strm
     , CC.SCTN_COMBINED_ID as class_number
     , CC.session_code
     , 'y' as is_combined
     , CI.emplid
     , 'E' as enroll_status
     , Case
           When CI.instr_role = 'TA' Then 'T'
           Else 'I' End as enroll_type
From PS.CS_PS_CLASS_INSTR CI
Inner Join ENTITY_LOCK EL
           On CI.strm = EL.term
Inner Join PS_COMBINED_CLASSES CC
           On CI.crse_id = CC.crse_id
               And CI.crse_offer_nbr = CC.crse_offer_nbr
               And CI.class_section = CC.class_section
               And CI.strm = CC.STRM
               And nvl(CI.session_code, '<null>') = nvl(CC.SESSION_CODE, '<null>')
Group By CC.SCTN_COMBINED_ID, CC.strm, CC.institution, CC.session_code, CI.emplid, CI.instr_role;
