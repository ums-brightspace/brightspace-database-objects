Create or Replace View PS_COLLEGE as
Select CT.INSTITUTION
     , CT.COLLEGE
From COLLEGE_TRANSLATION CT
Group By CT.INSTITUTION, CT.COLLEGE;
