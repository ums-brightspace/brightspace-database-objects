CREATE OR REPLACE FORCE VIEW "LMS_SANDBOXES_NEW" AS
SELECT S.emplid
From PS_SANDBOX S
Inner Join USER_COMPLETED UC
           on S.EMPLID = UC.EMPLID
Where S.emplid not in
      (
          Select emplid
          From SANDBOX_COMPLETED
      )
  And rownum < 41;
