CREATE OR REPLACE FORCE VIEW lms_roles AS
Select UC.LMS_ID user_lms_id,
       Case When RO.USER_LMS_ID Is Not Null THEN
            Case When R.ROLE_IND in ('P', 'N') Then
                R.ROLE_IND
            Else
                RO.ROLE_IND
           End
        Else
            R.ROLE_IND
        End role_ind
    , UC.EMPLID
From PS_ROLE R
Inner Join USER_COMPLETED UC
    On R.EMPLID = UC.EMPLID
Left Outer Join ROLE_OVERRIDE RO
    On UC.LMS_ID = RO.USER_LMS_ID
;
