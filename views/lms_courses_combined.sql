Create Or Replace Force View "LMS_COURSES_COMBINED" As
Select CC.code
     , CC.TITLE
     , CLASSES.parents
     , DECODE(substr(CC.STRM, 3, 1), '1', UC.TEMPLATE_LMS_ID_FALL, '2', UC.TEMPLATE_LMS_ID_SPRING, '3',
              UC.TEMPLATE_LMS_ID_SUMMER, null) as master
     , SC.LMS_ID as semester
     , CC.SCTN_COMBINED_ID as class_number
     , CC.INSTITUTION
     , CC.strm as term
     , 'y' as is_combined
     , CC.SESSION_CODE
     , CC.START_DT
     , CC.END_DT
From PS_COMBINED_COURSES CC
Inner Join ENTITY_LOCK EL
           On CC.strm = EL.TERM
INNER JOIN TERMS_TO_PROCESS TTP
           On CC.INSTITUTION = TTP.INSTITUTION
               And CC.strm = TTP.term
INNER JOIN SEMESTER_COMPLETED SC
           On CC.STRM = SC.CODE
Inner Join UNIVERSITY_COMPLETED UC
           On CC.INSTITUTION = substr(UC.CODE, 1, 5)
INNER JOIN
(
    Select INSTITUTION
         , STRM
         , SESSION_CODE
         , SCTN_COMBINED_ID
         , listagg(LMS_ID, '|') WITHIN GROUP ( ORDER BY LMS_ID) as parents
    From (
        Select CC.institution
             , CC.strm
             , CC.session_code
             , CC.SCTN_COMBINED_ID
             , UC.lms_id
        From PS_COMBINED_CLASSES CC
        Inner Join college_translation COL
                   On CC.institution = COL.institution
                       And CC.acad_group = COL.acad_group
                       And CC.acad_org = COL.acad_org
                       And CC.subject = COL.subject
        Inner Join UNIT_TRANSLATION UT
                   On CC.institution = UT.INSTITUTION
                       And CC.acad_group = nvl(UT.acad_group, CC.acad_group)
                       And CC.acad_org = nvl(UT.acad_org, CC.acad_org)
                       And CC.subject = nvl(UT.subject, CC.subject)
        Inner Join UNIT_COMPLETED UC
                   On COL.COLLEGE || '_' || UT.UNIT = UC.CODE
                       And UC.PARENT = (
                           Select LMS_ID
                           From COLLEGE_COMPLETED inCC
                           Where inCC.CODE = COL.COLLEGE
                       )
        GROUP BY CC.institution
               , CC.strm
               , CC.session_code
               , CC.SCTN_COMBINED_ID
               , UC.LMS_ID
    )
    group by INSTITUTION, STRM, SESSION_CODE, SCTN_COMBINED_ID
) CLASSES
On CC.INSTITUTION = CLASSES.INSTITUTION
    And CC.STRM = CLASSES.STRM
    And nvl(CC.SESSION_CODE, '<null>') = nvl(CLASSES.SESSION_CODE, '<null>')
    And CC.SCTN_COMBINED_ID = CLASSES.SCTN_COMBINED_ID;
