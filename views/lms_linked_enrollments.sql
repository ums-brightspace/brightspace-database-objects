CREATE OR REPLACE FORCE VIEW "LMS_LINKED_ENROLLMENTS" AS
Select LCP.INSTITUTION
     , LCP.TERM
     , LCP.CLASS_NUMBER
     , LCP.SESSION_CODE
     , LCP.IS_COMBINED
     , E.EMPLID
     , Case
           When Sum(Case When E.ENROLL_STATUS = 'E' Then 1 Else 0 End) > 0 Then
               'E'
           Else
               'D'
    End as ENROLL_STATUS
     , substr(
            (listagg(
                    Case
                        When ENROLL_STATUS = 'E' then ENROLL_TYPE
                        Else ''
                        End
                , '') within group ( order by decode(ENROLL_TYPE, 'I', 1, 'N', 2, 'T', 3, 'X', 4, 'S', 5))) || 'S'
    , 1
    , 1
    ) as ENROLL_TYPE
From LINKED_COURSE_PARENT LCP
Inner Join ENTITY_LOCK EL
    On LCP.TERM = EL.TERM
Inner Join TERMS_TO_PROCESS TTP
           On LCP.TERM = TTP.TERM
               And LCP.INSTITUTION = TTP.INSTITUTION
INNER JOIN LINKED_COURSE_CHILD LCC
           On LCP.CODE = LCC.PARENT_CODE
INNER JOIN PS_ENROLLMENTS E
           On LCC.CLASS_NUMBER = E.CLASS_NUMBER
               And LCC.INSTITUTION = E.INSTITUTION
               And nvl(LCC.SESSION_CODE, '<null>') = nvl(E.SESSION_CODE, '<null>')
               And LCC.TERM = E.STRM
               And E.IS_COMBINED = LCC.IS_COMBINED
Group By LCP.INSTITUTION, LCP.TERM, LCP.CLASS_NUMBER, LCP.SESSION_CODE, LCP.IS_COMBINED, E.EMPLID;
