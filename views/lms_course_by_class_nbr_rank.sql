CREATE OR REPLACE FORCE VIEW LMS_COURSE_BY_CLASS_NBR_RANK AS
Select LCP.CLASS_NUMBER class_nbr
     , LCP.INSTITUTION
     , LCP.TERM strm
     , max(CC.LMS_ID) as LMS_ID
     , 1 as rank
From LINKED_COURSE_PARENT LCP
Inner Join COURSE_COMPLETED CC
           On LCP.CODE = CC.CODE
Group By LCP.CLASS_NUMBER, LCP.INSTITUTION, LCP.TERM

Union

Select LCC.CLASS_NUMBER class_nbr
     , LCC.INSTITUTION
     , LCC.TERM strm
     , max(CC.LMS_ID) as LMS_ID
     , 2 as rank
From LINKED_COURSE_CHILD LCC
Inner Join LINKED_COURSE_PARENT LCP
           On LCC.PARENT_CODE = LCP.CODE
Inner Join COURSE_COMPLETED CC
           On LCP.CODE = CC.CODE
Group By LCC.CLASS_NUMBER, LCC.INSTITUTION, LCC.TERM

Union

Select to_char(PCC.CLASS_NBR) as class_nbr
     , PCC.INSTITUTION
     , PCC.STRM
     , CC.LMS_ID
     , 3 as rank
From PS_COMBINED_CLASSES_O PCC
Inner Join COURSE_COMPLETED CC
           On PCC.INSTITUTION = CC.INSTITUTION
               And PCC.STRM = CC.TERM
               And PCC.SCTN_COMBINED_ID = CC.CLASS_NUMBER
               And CC.IS_COMBINED = 'y'
               And nvl(PCC.SESSION_CODE, '<NULL>') = nvl(CC.SESSION_CODE, '<NULL>')

Union

Select to_char(CO.CLASS_NUMBER) class_nbr
     , CO.INSTITUTION
     , CO.TERM strm
     , CC.LMS_ID
     , 4 as rank
From PS_COURSE_OFFERING_O CO
Inner Join COURSE_COMPLETED CC
           On CO.INSTITUTION = CC.INSTITUTION
               And CO.TERM = CC.TERM
               And CO.CLASS_NUMBER = CC.CLASS_NUMBER
               And CC.IS_COMBINED = 'n'
;
