Create Or Replace Force View "PS_COURSE_OFFERING" As
Select CT.strm || '.' || CT.institution || '-S.' || CT.class_nbr || '.' || CT.session_code as code
     , trim(CT.subject) || ' ' || trim(CT.catalog_nbr) || ':' || trim(CT.class_section) || '-' || trim(CT.DESCR) ||
       ' (' || S.NAME || ')' As title
     , CT.class_nbr as class_number
     , CT.institution
     , CT.strm as term
     , 'n' as is_combined
     , CT.session_code
     , CT.acad_group
     , CT.acad_org
     , CT.subject
     , CT.start_dt
     , CT.end_dt
     , CT.catalog_nbr
From PS.CS_PS_CLASS_TBL CT
Inner Join ENTITY_LOCK EL
           On CT.strm = EL.term
Inner Join TERMS_TO_PROCESS TTP
           On CT.institution = TTP.INSTITUTION
               And CT.strm = TTP.term
Inner Join PS_SEMESTER S
           On CT.STRM = S.TERM
Where (CT.class_nbr, CT.institution, CT.strm) Not In
      (
          Select class_nbr, institution, strm
          From PS.CS_PS_SCTN_CMBND
      )
  And CT.class_stat <> 'X';
