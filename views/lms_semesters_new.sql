CREATE OR REPLACE FORCE VIEW "LMS_SEMESTERS_NEW" AS
SELECT DISTINCT term as code
              , name
From PS_SEMESTER S
Where S.TERM in
      (
          Select term
          From PS_SEMESTER

          MINUS

          Select code
          From SEMESTER_COMPLETED
      );