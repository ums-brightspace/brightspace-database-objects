CREATE OR REPLACE FORCE VIEW "LMS_UNITS_NEW" AS
SELECT U.COLLEGE || '_' || U.unit as code,
       U.COLLEGE || ' ' || U.unit as name,
       CC.LMS_ID as parent
From PS_UNIT U
Inner Join COLLEGE_COMPLETED CC
           On U.COLLEGE = CC.CODE
Where (U.COLLEGE, U.UNIT) in
      (
          Select COLLEGE, UNIT
          From PS_UNIT

          MINUS

          Select wCC.CODE, substr(wUC.CODE, instr(wUC.CODE, '_UMS') + 1)
          From UNIT_COMPLETED wUC
          Inner Join COLLEGE_COMPLETED wCC
                     On wUC.PARENT = wCC.LMS_ID
      )
  And rownum < 11;
