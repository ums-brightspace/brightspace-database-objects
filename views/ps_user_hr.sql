create or replace view ps_user_hr
as
Select EMP.emplid
     , EMP.campus
     , EMP.username
     , EMP.email
     , N.LAST_NAME
     , N.FIRST_NAME
     , N.MIDDLE_NAME
From (
    --Current Employees
    select s.emplid
         , s.business_unit as campus
         , substr(s.email_addr, 1, instr(s.email_addr, '@', 1, 1) - 1) as username
         , s.email_addr as email
    from PS.CS_PS_UM_FGLEMPLOY_VW s
    where s.empl_status IN ('A', 'L', 'P')
      and s.primary_job_ind = 'Y'
      and s.email_addr is not null
      and s.email_addr <> ' '
      And lower(s.email_addr) like '%@maine.edu'
    UNION

    --Future Employees
    Select JOB.emplid
         , JOB.business_unit as campus
         , substr(EMAIL.email_addr, 1, instr(EMAIL.email_addr, '@', 1, 1) - 1) as username
         , EMAIL.email_addr as email
    From PS.HR_PS_UM_ID_JOB_VW JOB
    Inner Join PS.HR_PS_UM_ID_EMAIL_VW EMAIL
               On JOB.emplid = EMAIL.emplid
    Where trunc(begin_dt) > trunc(current_date)
      And empl_status in ('A', 'L', 'P')
      And EMAIL.e_addr_type = 'BUSN'
      And lower(EMAIL.email_addr) like '%@maine.edu'

    UNION

    --Persons of Interest
    SELECT POI.emplid
         , min(POI.business_unit) as campus
         , substr(min(EA.email_addr), 1, instr(min(EA.email_addr), '@', 1, 1) - 1) as username
         , min(EA.email_addr) as email
    FROM PS.HR_PS_UM_PER_POI POI
    Inner Join PS.HR_PS_EMAIL_ADDRESSES EA
               On POI.emplid = EA.emplid
                   And POI.UM_POI_INSTANCE = 1
                   And EA.email_addr is not null
                   and EA.email_addr <> ' '
                   And EA.e_addr_type = 'BUSN'
                   And lower(EA.email_addr) like '%@maine.edu'
                   And trunc(current_date) >= nvl(POI.begin_dt, trunc(current_date))
                   And trunc(current_date) <= nvl(POI.end_dt, trunc(current_date))
    Where POI.emplid not in (
        Select emplid
        From PS.CS_PS_UM_FGLEMPLOY_VW
        Where empl_status IN ('A', 'L', 'P')
          and primary_job_ind = 'Y'
          and email_addr is not null
          and email_addr <> ' '
          And lower(email_addr) like '%@maine.edu'
    )
    Group By POI.emplid
) EMP
Inner Join PS_USER_NAME_HR N
           On EMP.emplid = N.EMPLID;