Create or Replace Force View "PS_USER_CAMPUS" as
    --     SELECT P.EMPLID
--          , SE.INSTITUTION as campus
--     From sysadm.ps_person@ADM_TO_CS P
--     INNER JOIN sysadm.ps_stdnt_enrl@ADM_TO_CS SE
--                On P.EMPLID = SE.EMPLID
--
--     UNION
--
--     SELECT P.EMPLID
--          , CT.INSTITUTION
--     From sysadm.ps_person@ADM_TO_CS P
--     INNER JOIN sysadm.PS_CLASS_INSTR@ADM_TO_CS CI
--                On P.EMPLID = CI.EMPLID
--     Inner JOIN sysadm.PS_CLASS_TBL@ADM_TO_CS CT
--                On CI.CRSE_ID = CT.CRSE_ID
--                    And CI.CRSE_OFFER_NBR = CT.CRSE_OFFER_NBR
--                    And CI.STRM = CT.STRM
--                    And CI.SESSION_CODE = CT.SESSION_CODE
--                    And CI.CLASS_SECTION = CT.CLASS_SECTION;

Select P.emplid
     , 'UMS22' as campus
From PS.CS_PS_PERSON P;