CREATE OR REPLACE FORCE VIEW "LMS_COLLEGES_NEW" AS
SELECT DISTINCT C.college as code,
                C.college as name,
                UC.LMS_ID as parent
From PS_COLLEGE C
Inner Join UNIVERSITY_COMPLETED UC
           On C.INSTITUTION || '_university' = UC.CODE
Where C.COLLEGE in
      (
          Select COLLEGE
          From PS_COLLEGE

          MINUS

          Select CODE
          From COllEGE_COMPLETED
      )
  And rownum < 11;