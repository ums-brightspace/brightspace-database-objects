Create or Replace Force View "PS_USER_NAME_CS_PRIMARY" as
SELECT PRI.emplid
     , PRI.first_name
     , PRI.last_name
     , PRI.middle_name
From PS.CS_PS_NAMES PRI
Where PRI.name_type = 'PRI'
  And PRI.eff_status = 'A'
  and PRI.effdt =
      (
          Select max(effdt)
          From PS.CS_PS_NAMES inN
          Where inN.emplid = PRI.emplid
            And inN.effdt <= trunc(sysdate)
            And inN.eff_status = 'A'
            And inN.name_type = 'PRI'
      );
