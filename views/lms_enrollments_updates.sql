CREATE OR REPLACE FORCE VIEW "LMS_ENROLLMENTS_UPDATES" AS
Select E.user_lms_id
     , E.course_lms_id
     , E.type
     , E.enroll_status
From LMS_ENROLLMENTS E
Inner Join ENROLLMENT_COMPLETED EC
           On E.USER_LMS_ID = EC.USER_LMS_ID
               And E.COURSE_LMS_ID = EC.COURSE_LMS_ID
               And (
                      nvl(E.TYPE, '<null>') <> nvl(EC.TYPE, '<null>')
                      Or nvl(E.ENROLL_STATUS, '<null>') <> nvl(EC.ENROLL_STATUS, '<null>')
                  )
Group By E.user_lms_id, E.course_lms_id, E.type, E.enroll_status;
