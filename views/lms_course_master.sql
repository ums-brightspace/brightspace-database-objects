Create Or Replace Force View "LMS_COURSE_MASTER" As
Select DECODE(substr(CO.TERM, 3, 1),
              '1',
              nvl(UNIT_COM.TEMPLATE_LMS_ID_FALL, nvl(COLL_COM.TEMPLATE_LMS_ID_FALL, UNIV_COM.TEMPLATE_LMS_ID_FALL)),
              '2', nvl(UNIT_COM.TEMPLATE_LMS_ID_SPRING,
                       nvl(COLL_COM.TEMPLATE_LMS_ID_SPRING, UNIV_COM.TEMPLATE_LMS_ID_SPRING)),
              '3', nvl(UNIT_COM.TEMPLATE_LMS_ID_SUMMER,
                       nvl(COLL_COM.TEMPLATE_LMS_ID_SUMMER, UNIV_COM.TEMPLATE_LMS_ID_SUMMER)),
              null) as master
     , to_char(CO.CLASS_NUMBER) as class_number
     , CO.INSTITUTION
     , CO.TERM
     , CO.IS_COMBINED
     , CO.SESSION_CODE
     , CO.code
From PS_COURSE_OFFERING_O CO
Inner Join college_translation COL
           On CO.institution = COL.institution
               And CO.acad_group = COL.acad_group
               And CO.acad_org = COL.acad_org
               And CO.subject = COL.subject
Inner Join UNIT_TRANSLATION UT
           On CO.institution = UT.INSTITUTION
               And CO.acad_group = nvl(UT.acad_group, CO.acad_group)
               And CO.acad_org = nvl(UT.acad_org, CO.acad_org)
               And CO.subject = nvl(UT.subject, CO.subject)
Inner Join COLLEGE_COMPLETED COLL_COM
           On COL.college = COLL_COM.CODE
Inner Join UNIT_COMPLETED UNIT_COM
           On COL.COLLEGE || '_' || UT.UNIT = UNIT_COM.CODE
               And UNIT_COM.PARENT = COLL_COM.LMS_ID
Inner Join UNIVERSITY_COMPLETED UNIV_COM
           On CO.institution = substr(UNIV_COM.CODE, 1, 5)

Union

Select DECODE(substr(CC.STRM, 3, 1), '1', UC.TEMPLATE_LMS_ID_FALL, '2', UC.TEMPLATE_LMS_ID_SPRING, '3',
              UC.TEMPLATE_LMS_ID_SUMMER, null) as master
     , CC.SCTN_COMBINED_ID as class_number
     , CC.INSTITUTION
     , CC.strm as term
     , 'y' as is_combined
     , CC.SESSION_CODE
     , CC.CODE
From PS_COMBINED_COURSES_O CC
Inner Join UNIVERSITY_COMPLETED UC
           On CC.INSTITUTION = substr(UC.CODE, 1, 5);
