Create or Replace View PS_SEMESTER as
Select CT.strm term
     , (
    Case substr(CT.strm, 3, 1)
        When '1' Then
            '20' || to_char((to_number(substr(CT.strm, 1, 2)) - 1)) || ' Fall'
        When '2' Then
            '20' || substr(CT.strm, 1, 2) || ' Spring'
        When '3' Then
            '20' || substr(CT.strm, 1, 2) || ' Summer'
        Else
            ''
        END
    ) as name
From PS.CS_PS_CLASS_TBL CT
Where CT.strm > 1100
Group By CT.strm;
