CREATE OR REPLACE FORCE VIEW lms_enrollments_by_class
AS
Select ENROLL.INSTITUTION
     , ENROLL.term
     , ENROLL.CLASS_NUMBER
     , ENROLL.IS_COMBINED
     , ENROLL.SESSION_CODE
     , ENROLL.USER_LMS_ID
     , ENROLL.type
     , ENROLL.EMPLID
     , ENROLL.FIRST_NAME
     , ENROLL.LAST_NAME
     , ENROLL.USERNAME
     , ENROLL.ENROLL_STATUS
     , CC.LMS_ID as course_lms_id
From (
    Select INSTITUTION
         , term
         , CLASS_NUMBER
         , IS_COMBINED
         , SESSION_CODE
         , EMPLID
         , USER_LMS_ID
         , FIRST_NAME
         , LAST_NAME
         , USERNAME
         , Case
               When Sum(Case When ENROLL_STATUS = 'E' Then 1 Else 0 End) > 0 Then
                   'E'
               Else
                   'D'
        End ENROLL_STATUS
         -- This is a not so clever hack to limit the enrollments to a single record per class/person.
         -- This will determine the role/type by getting the enrolled row for that user/class.
         -- If there is more than 1, then decode will pick the highest ranked.
         -- If there are no enrolled rows (a delete), then learner (S) is assumed.
         -- This is needed to fix BSI-196.
         , substr(
                (listagg(
                        Case
                            When ENROLL_STATUS = 'E' then TYPE
                            Else ''
                            End
                    , '') within group ( order by decode(TYPE, 'I', 1, 'N', 2, 'T', 3, 'X', 4, 'S', 5))) || 'S'
        , 1
        , 1
        ) as TYPE
    From (
        Select INSTITUTION
             , term
             , CLASS_NUMBER
             , IS_COMBINED
             , SESSION_CODE
             , USER_LMS_ID
             , type
             , EMPLID
             , FIRST_NAME
             , LAST_NAME
             , USERNAME
             , ENROLL_STATUS
        From LMS_STUDENTS_BY_CLASS

        Union

        Select LCP.INSTITUTION
             , LCP.term
             , LCP.CLASS_NUMBER
             , LCP.IS_COMBINED
             , LCP.SESSION_CODE
             , USER_LMS_ID
             , type
             , EMPLID
             , FIRST_NAME
             , LAST_NAME
             , USERNAME
             , ENROLL_STATUS
        From LMS_STUDENTS_BY_CLASS SBC
        Inner Join LINKED_COURSE_CHILD LCC
                   On SBC.INSTITUTION = LCC.INSTITUTION
                       And SBC.TERM = LCC.TERM
                       And SBC.CLASS_NUMBER = LCC.CLASS_NUMBER
                       And SBC.IS_COMBINED = LCC.IS_COMBINED
                       And nvl(SBC.SESSION_CODE, '<null>') = nvl(LCC.SESSION_CODE, '<null>')
        Inner Join LINKED_COURSE_PARENT LCP
                   On LCC.PARENT_CODE = LCP.CODE

        UNION

        Select INSTITUTION
             , TERM
             , CLASS_NUMBER
             , IS_COMBINED
             , SESSION_CODE
             , USER_LMS_ID
             , TYPE
             , EMPLID
             , first_name
             , last_name
             , USERNAME
             , 'E' as enroll_status
        From LMS_INSTRUCTORS_BY_CLASS

        Union

        Select LCP.INSTITUTION
             , LCP.TERM
             , LCP.CLASS_NUMBER
             , LCP.IS_COMBINED
             , LCP.SESSION_CODE
             , USER_LMS_ID
             , TYPE
             , EMPLID
             , first_name
             , last_name
             , USERNAME
             , 'E' as enroll_status
        From LMS_INSTRUCTORS_BY_CLASS IBC
        Inner Join LINKED_COURSE_CHILD LCC
                   On IBC.INSTITUTION = LCC.INSTITUTION
                       And IBC.TERM = LCC.TERM
                       And IBC.CLASS_NUMBER = LCC.CLASS_NUMBER
                       And IBC.IS_COMBINED = LCC.IS_COMBINED
                       And nvl(IBC.SESSION_CODE, '<null>') = nvl(LCC.SESSION_CODE, '<null>')
        Inner Join LINKED_COURSE_PARENT LCP
                   On LCC.PARENT_CODE = LCP.CODE
    )
    Group By INSTITUTION, term, CLASS_NUMBER, IS_COMBINED, SESSION_CODE
           , EMPLID, USER_LMS_ID, FIRST_NAME, LAST_NAME, USERNAME
) ENROLL
Left Outer Join COURSE_COMPLETED CC
                On ENROLL.INSTITUTION = CC.INSTITUTION
                    And ENROLL.TERM = CC.TERM
                    And ENROLL.CLASS_NUMBER = CC.CLASS_NUMBER
                    And ENROLL.IS_COMBINED = CC.IS_COMBINED
                    And nvl(ENROLL.SESSION_CODE, '<null>') = nvl(CC.SESSION_CODE, '<null>')
Order By LAST_NAME, FIRST_NAME, EMPLID;
