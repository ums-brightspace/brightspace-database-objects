CREATE OR REPLACE FORCE VIEW lms_students_by_class
AS
Select S.institution
     , S.strm as term
     , S.class_number
     , S.session_code
     , S.is_combined
     , S.EMPLID
     , S.enroll_type as type
     , S.enroll_status
     , UC.LMS_ID as user_lms_id
     , UC.FIRST_NAME
     , UC.MIDDLE_NAME
     , UC.LAST_NAME
     , UC.USERNAME
From (
    Select SE.institution
         , SE.strm
         , to_char(SE.class_nbr) as class_number
         , SE.session_code
         , 'n' as is_combined
         , SE.emplid
         , Case
               When Sum(Case When SE.stdnt_enrl_status = 'E' And SE.enrl_status_reason <> 'WDRW' Then 1 Else 0 End) > 0
                   Then 'E'
               Else 'D' End as enroll_status
         , Case
               When Sum(Case
                            When SE.crse_grade_off = 'I' Or (SE.institution = 'UMS03' And SE.crse_grade_off = 'DG') Or
                                 (SE.institution = 'UMS07' And SE.crse_grade_off = 'NP') Then 1
                            Else 0 End) > 0 Then
                   'X'
               Else
                   'S'
        End as enroll_type
    From PS.CS_PS_STDNT_ENRL SE
    Where SE.strm > 1100
    Group By SE.institution, SE.strm, SE.class_nbr, SE.session_code, SE.emplid

    UNION

    Select SE.institution
         , SE.strm
         , CC.SCTN_COMBINED_ID as class_number
         , SE.session_code
         , 'y' as is_combined
         , SE.emplid
         , Case
               When Sum(Case When SE.stdnt_enrl_status = 'E' And SE.enrl_status_reason <> 'WDRW' Then 1 Else 0 End) > 0
                   Then 'E'
               Else 'D' End as enroll_status
         , Case
               When Sum(Case
                            When SE.crse_grade_off = 'I' Or (SE.institution = 'UMS03' And SE.crse_grade_off = 'DG') Or
                                 (SE.institution = 'UMS05' And SE.crse_grade_off = 'DG') Or
                                 (SE.institution = 'UMS07' And SE.crse_grade_off = 'NP') Then 1
                            Else 0 End) > 0 Then
                   'X'
               Else
                   'S'
        End as enroll_type
    From PS.CS_PS_STDNT_ENRL SE
    Inner Join PS_COMBINED_CLASSES_O CC
               On SE.strm > 1100
                   And SE.institution = CC.INSTITUTION
                   And SE.strm = CC.STRM
                   And SE.class_nbr = CC.CLASS_NBR
                   And nvl(SE.session_code, '<null>') = nvl(CC.SESSION_CODE, '<null>')
    Group By CC.SCTN_COMBINED_ID, SE.strm, SE.institution, SE.session_code, SE.emplid
) S
Inner Join USER_COMPLETED UC
           On S.EMPLID = UC.EMPLID;
