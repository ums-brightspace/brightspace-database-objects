CREATE OR REPLACE FORCE VIEW ps_enroll_regular_student
AS
Select SE.institution
     , SE.strm
     , SE.class_nbr as class_number
     , SE.session_code
     , 'n' as is_combined
     , SE.emplid
     , Case
           When Sum(Case When SE.stdnt_enrl_status = 'E' And SE.enrl_status_reason <> 'WDRW' Then 1 Else 0 End) > 0
               Then 'E'
           Else 'D' End as enroll_status
     , Case
           When Sum(Case
                        When SE.crse_grade_off = 'I' Or (SE.institution = 'UMS03' And SE.crse_grade_off = 'DG') Or
                             (SE.institution = 'UMS05' And SE.crse_grade_off = 'DG') Or
                             (SE.institution = 'UMS07' And SE.crse_grade_off = 'NP') Then 1
                        Else 0 End) > 0 Then
               Case
                   When Sum(
                                Case
                                    When nvl(SG.stdnt_group, '<null>') = '1DOC' Then 1
                                    Else 0 End) > 0 Then
                       'Q'
                   Else
                       'X'
                   End
           When Sum(
                        Case When nvl(SG.stdnt_group, '<null>') = '1DOC' Then 1
                             Else 0 End ) > 0 Then
               'P'
           Else
               'S'
    End as enroll_type
From PS.CS_PS_STDNT_ENRL SE
Inner Join ENTITY_LOCK EL
           On SE.strm = EL.term
Inner Join TERMS_TO_PROCESS TTP
           On SE.institution = TTP.INSTITUTION
               And SE.strm = TTP.TERM
Left Outer Join PS.CS_PS_STDNT_GRPS SG
                On SE.emplid = SG.emplid
                    And SG.institution = 'UMS01'
                    And SG.stdnt_group = '1DOC'
Group By SE.institution, SE.strm, SE.class_nbr, SE.session_code, SE.emplid;
