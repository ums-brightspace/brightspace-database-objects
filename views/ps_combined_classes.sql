Create Or Replace Force View "PS_COMBINED_CLASSES" As
Select SCT.institution
     , SCT.strm
     , SCT.session_code
     , CT.class_nbr
     , SCT.sctn_combined_id
     , CT.acad_group
     , CT.acad_org
     , CT.subject
     , CT.crse_id
     , CT.crse_offer_nbr
     , CT.class_section
     , CT.DESCR
     , CT.CATALOG_NBR
     , CT.start_dt
     , CT.end_dt
From PS.CS_PS_SCTN_CMBND_TBL SCT
Inner Join ENTITY_LOCK EL
           On SCT.strm = EL.term
Inner Join TERMS_TO_PROCESS TTP
           On SCT.institution = TTP.INSTITUTION
               And SCT.strm = TTP.term
INNER JOIN PS.CS_PS_SCTN_CMBND SC
           On SCT.institution = SC.institution
               And SCT.strm = SC.strm
               And nvl(SCT.session_code, '<null>') = nvl(SC.session_code, '<null>')
               And SCT.sctn_combined_Id = SC.sctn_combined_id
Inner Join PS.CS_PS_CLASS_TBL CT
           On SCT.institution = CT.institution
               And SCT.strm = CT.strm
               And nvl(SCT.session_code, '<null>') = nvl(CT.session_code, '<null>')
               And SC.class_nbr = CT.class_nbr;
