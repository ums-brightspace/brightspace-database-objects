Create or Replace View lms_course_issue_no_instructor
AS
Select CC.code as code
     , CC.TITLE as title
     , CC.INSTITUTION as institution
     , cc.TERM as term
     , CC.CLASS_NUMBER as class_number
     , CC.SESSION_CODE as session_code
     , CC.IS_COMBINED as is_combined
     , 'no_instructor' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
               And CC.IS_COMBINED = 'n'
Inner Join COURSE_SETTINGS CS
           On CC.INSTITUTION = CS.INSTITUTION
Left Outer Join PS_ENROLL_REGULAR_INSTRUCTOR EI
                On CC.INSTITUTION = EI.institution
                    And CC.term = EI.strm
                    And CC.CLASS_NUMBER = EI.CLASS_NUMBER
                    And nvl(CC.SESSION_CODE, '<NULL>') = nvl(EI.session_code, '<NULL>')
Where EI.CLASS_NUMBER is null
  And sysdate >= (
    Case
        When CS.SET_START_DATE = 1 Then
            CC.START_DT + CS.START_DATE_DAYS_OFFSET
        Else
            CC.START_DT
        End
    )
  And exists(
        Select ES.EMPLID
        From PS_ENROLL_REGULAR_STUDENT ES
        Where ES.CLASS_NUMBER = CC.CLASS_NUMBER
          And ES.INSTITUTION = CC.INSTITUTION
          And ES.STRM = CC.TERM
          And nvl(ES.SESSION_CODE, '<NULL>') = nvl(CC.SESSION_CODE, '<NULL>')
    )

Union All

Select CC.code as code
     , CC.TITLE as title
     , CC.INSTITUTION as institution
     , cc.TERM as term
     , CC.CLASS_NUMBER as class_number
     , CC.SESSION_CODE as session_code
     , CC.IS_COMBINED as is_combined
     , 'no_instructor' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
               And CC.IS_COMBINED = 'y'
Inner Join COURSE_SETTINGS CS
           On CC.INSTITUTION = CS.INSTITUTION
Left Outer Join PS_ENROLL_COMBINED_INSTRUCTOR EI
                On CC.INSTITUTION = EI.institution
                    And CC.term = EI.strm
                    And CC.CLASS_NUMBER = EI.CLASS_NUMBER
                    And nvl(CC.SESSION_CODE, '<NULL>') = nvl(EI.session_code, '<NULL>')
Where EI.CLASS_NUMBER is null
  And sysdate >= (
    Case
        When CS.SET_START_DATE = 1 Then
            CC.START_DT + CS.START_DATE_DAYS_OFFSET
        Else
            CC.START_DT
        End
    )
  And exists(
        Select ES.EMPLID
        From PS_ENROLL_COMBINED_STUDENT ES
        Where ES.CLASS_NUMBER = CC.CLASS_NUMBER
          And ES.INSTITUTION = CC.INSTITUTION
          And ES.STRM = CC.TERM
          And nvl(ES.SESSION_CODE, '<NULL>') = nvl(CC.SESSION_CODE, '<NULL>')
    )

MINUS

Select CI.CODE
     , CI.TITLE
     , CI.INSTITUTION
     , CI.TERM
     , CI.CLASS_NUMBER
     , CI.SESSION_CODE
     , CI.IS_COMBINED
     , CI.ISSUE
From COURSE_ISSUES CI
Where CI.ISSUE = 'no_instructor'
;
