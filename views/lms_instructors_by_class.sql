CREATE OR REPLACE FORCE VIEW "LMS_INSTRUCTORS_BY_CLASS" AS
Select UC.LMS_ID as user_lms_id
     , I.EMPLID
     , I.INSTITUTION
     , I.STRM as term
     , I.CLASS_NUMBER
     , I.SESSION_CODE
     , I.IS_COMBINED
     , enroll_type as type
     , UC.FIRST_NAME
     , UC.LAST_NAME
     , UC.USERNAME
From (
    Select CT.institution
         , CT.strm
         , to_char(CT.class_nbr) as class_number
         , CT.session_code
         , 'n' as is_combined
         , CI.emplid
         , Case When CI.instr_role = 'TA' Then 'T' Else 'I' End as enroll_type
    From PS.CS_PS_CLASS_INSTR CI
    Inner Join PS.CS_PS_CLASS_TBL CT
               On CI.strm = CT.strm
                   And nvl(CI.session_code, '<null>') = nvl(CT.SESSION_CODE, '<null>')
                   and CI.class_section = CT.CLASS_SECTION
                   And CI.CRSE_ID = CT.CRSE_ID
                   And CI.CRSE_OFFER_NBR = CT.CRSE_OFFER_NBR
                   And CI.instr_role in ('PI', 'SI')
                   And CI.strm > 1100

    Union

    Select CT.institution
         , CT.strm
         , SCT.sctn_combined_id as class_number
         , CT.session_code
         , 'y' as is_combined
         , CI.emplid
         , Case When CI.instr_role = 'TA' Then 'T' Else 'I' End as enroll_type
    From PS.CS_PS_SCTN_CMBND_TBL SCT
    INNER JOIN PS.CS_PS_SCTN_CMBND SC
               On SCT.institution = SC.institution
                   And SCT.strm = SC.strm
                   And nvl(SCT.session_code, '<null>') = nvl(SC.session_code, '<null>')
                   And SCT.sctn_combined_Id = SC.sctn_combined_id
                   And SCT.strm > 1100
    Inner Join PS.CS_PS_CLASS_TBL CT
               On SCT.institution = CT.institution
                   And SCT.strm = CT.strm
                   And nvl(SCT.session_code, '<null>') = nvl(CT.session_code, '<null>')
                   And SC.class_nbr = CT.class_nbr
    Inner Join PS.CS_PS_CLASS_INSTR CI
               On CI.crse_id = CT.crse_id
                   And CI.crse_offer_nbr = CT.crse_offer_nbr
                   And CI.class_section = CT.class_section
                   And CI.strm = CT.STRM
                   And nvl(CI.session_code, '<null>') = nvl(CT.SESSION_CODE, '<null>')
                   And CI.instr_role in ('PI', 'SI')
) I
Inner Join USER_COMPLETED UC
           On I.EMPLID = UC.EMPLID;
