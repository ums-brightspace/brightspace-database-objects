Create or Replace Force View ps_combined_courses_title as
Select INSTITUTION
     , CSC.STRM
     , SCTN_COMBINED_ID
     , SESSION_CODE
     , (
        listagg(SUBJECT || ' ' || CATALOG_NBR, '/')
                WITHIN GROUP ( order by SUBJECT, CATALOG_NBR)
        || ': ' ||
        listagg(DESCR, '//')
                WITHIN GROUP ( order by SUBJECT, CATALOG_NBR)
        || ' (' || S.NAME || ')'
    ) As title
From (
    Select CC.INSTITUTION
         , CC.STRM
         , CC.SCTN_COMBINED_ID
         , CC.SESSION_CODE
         , trim(CC.SUBJECT) subject
         , trim(CC.CATALOG_NBR) CATALOG_NBR
         , trim(CC.DESCR) descr
    From PS_COMBINED_CLASSES CC
    Inner Join ENTITY_LOCK EL
        On CC.STRM = EL.term
    Group By CC.INSTITUTION, CC.STRM, CC.SCTN_COMBINED_ID, CC.SESSION_CODE, CC.SUBJECT, CC.CATALOG_NBR, CC.DESCR
) CSC
Inner Join PS_SEMESTER S
           On CSC.STRM = S.TERM
Group By INSTITUTION, CSC.STRM, S.NAME, SCTN_COMBINED_ID, SESSION_CODE