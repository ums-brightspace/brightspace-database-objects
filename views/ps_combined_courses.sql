Create Or Replace Force View "PS_COMBINED_COURSES" As
Select SCT.strm || '.' || SCT.institution || '-C.' || SCT.SCTN_COMBINED_ID || '.' || SCT.session_code as code
     , CCT.TITLE
     , SCT.institution
     , SCT.strm
     , SCT.session_code
     , SCT.sctn_combined_id
     , CSC.start_dt
     , CSC.end_dt
From PS.CS_PS_SCTN_CMBND_TBL SCT
Inner Join ENTITY_LOCK EL
           On SCT.strm = EL.term
Inner Join TERMS_TO_PROCESS TTP
           On SCT.institution = TTP.INSTITUTION
               And SCT.strm = TTP.term
Inner Join PS_COMBINED_COURSES_TITLE CCT
           On SCT.institution = CCT.institution
               And SCT.strm = CCT.strm
               And nvl(SCT.session_code, '<null>') = nvl(CCT.session_code, '<null>')
               And SCT.sctn_combined_Id = CCT.sctn_combined_id
Inner Join (
    Select CC.INSTITUTION
         , CC.STRM
         , CC.SCTN_COMBINED_ID
         , CC.SESSION_CODE
         , min(CC.START_DT) as start_dt
         , max(CC.END_DT) as end_dt
    From PS_COMBINED_CLASSES CC
    Group By CC.INSTITUTION, CC.STRM, CC.SCTN_COMBINED_ID, CC.SESSION_CODE
) CSC
           On SCT.institution = CSC.institution
               And SCT.strm = CSC.strm
               And nvl(SCT.session_code, '<null>') = nvl(CSC.session_code, '<null>')
               And SCT.sctn_combined_Id = CSC.sctn_combined_id;
