Create or Replace View lms_course_issue_duplicate
AS
Select CC.code as code
     , min(CC.TITLE) as title
     , min(CC.INSTITUTION) as institution
     , min(cc.TERM) as term
     , min(CC.CLASS_NUMBER) as class_number
     , min(CC.SESSION_CODE) as session_code
     , min(CC.IS_COMBINED) as is_combined
     , 'duplicate' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
Group By CC.CODE
Having count(*) > 1

UNION All

Select min(CC.CODE) as code
     , CC.TITLE as title
     , min(CC.INSTITUTION) as institution
     , min(cc.TERM) as term
     , min(CC.CLASS_NUMBER) as class_number
     , min(CC.SESSION_CODE) as session_code
     , min(CC.IS_COMBINED) as is_combined
     , 'duplicate' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
Group By CC.TITLE
Having count(*) > 1
   And min(CC.CODE) <> max(CC.CODE)

MINUS

Select CI.CODE
     , CI.TITLE
     , CI.INSTITUTION
     , CI.TERM
     , CI.CLASS_NUMBER
     , CI.SESSION_CODE
     , CI.IS_COMBINED
     , CI.ISSUE
From COURSE_ISSUES CI
Where CI.ISSUE = 'duplicate'
;
