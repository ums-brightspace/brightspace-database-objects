CREATE OR REPLACE FORCE VIEW lms_roles_new AS
Select USER_LMS_ID
    , role_ind
From (
    Select R.ROLE_IND
        , R.USER_LMS_ID
    From LMS_ROLES R
    Where not exists(
            Select 1
            From ROLE_COMPLETED RC
            Where RC.USER_LMS_ID = R.USER_LMS_ID
        )
)
Where rownum < 1001;
