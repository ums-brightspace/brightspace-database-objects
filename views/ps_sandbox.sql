Create or Replace View PS_SANDBOX as
Select CI.emplid
From PS.CS_PS_CLASS_INSTR CI
Where CI.strm > to_char(sysdate, 'YY') || '00'
Group By CI.emplid;
