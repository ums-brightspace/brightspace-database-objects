CREATE OR REPLACE FORCE VIEW "LMS_COURSE_INFO" AS
Select C.INSTITUTION
     , C.TERM
     , C.class_number
     , C.is_combined
     , C.SESSION_CODE
     , (
    Case
        When length(C.title) > 128 Then
                trim(substr(substr(C.title, 1, instr(C.title, '(', -1) - 2), 1, 110)) || '... ' ||
                substr(C.title, instr(C.title, '(', -1))
        Else
            C.title
        End
    ) as title
     , C.CODE
     , CC.LMS_ID
     , listagg(I.FIRST_NAME || ' ' || I.LAST_NAME, ', ')
               within group ( order by I.FIRST_NAME, I.LAST_NAME) as instructor_names
     , listagg(I.EMPLID, ', ') within group ( order by I.FIRST_NAME, I.LAST_NAME) as instructor_emplids
     , listagg(I.USERNAME, ', ') within group ( order by I.FIRST_NAME, I.LAST_NAME) as instructor_usernames
     , (
    Select CP.PARENTS
    From LMS_COURSE_PARENTS CP
    Where C.INSTITUTION = CP.INSTITUTION
      And C.TERM = CP.TERM
      And C.class_number = CP.CLASS_NUMBER
      And C.IS_COMBINED = CP.IS_COMBINED
      And nvl(C.SESSION_CODE, '<null>') = nvl(CP.SESSION_CODE, '<null>')
) as parents
     , (
    Select CM.MASTER
    From LMS_COURSE_MASTER CM
    Where C.INSTITUTION = CM.INSTITUTION
      And C.TERM = CM.TERM
      And C.class_number = CM.CLASS_NUMBER
      And C.IS_COMBINED = CM.IS_COMBINED
      And nvl(C.SESSION_CODE, '<null>') = nvl(CM.SESSION_CODE, '<null>')
) as master
    , C.ACAD_ORG
    , C.SUBJECT
    , C.CATALOG_NBR
From (
    Select CO.institution
         , CO.term
         , to_char(CO.CLASS_NUMBER) as class_number
         , CO.IS_COMBINED
         , CO.session_code
         , CO.title
         , CO.code
         , CO.ACAD_ORG
         , CO.SUBJECT
         , CO.CATALOG_NBR
    From PS_COURSE_OFFERING_O CO

    Union

    Select PCC.institution
         , PCC.strm as term
         , to_char(PCC.sctn_combined_id) as class_number
         , 'y' as is_combined
         , PCC.session_code
         , PCC.title
         , PCC.code
         , '' as acad_org
         , '' as subject
         , '' as catalog_nbr
    From PS_COMBINED_COURSES_O PCC
) C
Left Outer Join COURSE_COMPLETED CC
                On C.INSTITUTION = CC.INSTITUTION
                    And C.term = CC.term
                    And C.class_number = CC.CLASS_NUMBER
                    And C.is_combined = CC.IS_COMBINED
                    And nvl(C.SESSION_CODE, '<null>') = nvl(CC.SESSION_CODE, '<null>')
Left Outer Join LMS_INSTRUCTORS_BY_CLASS I
                On C.INSTITUTION = I.INSTITUTION
                    And C.term = I.term
                    And C.class_number = I.CLASS_NUMBER
                    And C.is_combined = I.IS_COMBINED
                    And nvl(C.SESSION_CODE, '<null>') = nvl(I.SESSION_CODE, '<null>')
Group By C.INSTITUTION
       , C.TERM
       , C.class_number
       , C.is_combined
       , C.SESSION_CODE
       , C.TITLE
       , C.CODE
       , CC.LMS_ID
       , C.ACAD_ORG
       , C.SUBJECT
       , C.CATALOG_NBR
Order By C.title;
