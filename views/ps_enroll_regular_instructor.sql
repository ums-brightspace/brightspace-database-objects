CREATE OR REPLACE FORCE VIEW ps_enroll_regular_instructor
AS
Select CT.institution
     , CT.strm
     , CT.class_nbr as class_number
     , CT.session_code
     , 'n' as is_combined
     , CI.emplid
     , 'E' as enroll_status
     , Case
           When CI.instr_role = 'TA' Then 'T'
           Else 'I' End as enroll_type
From PS.CS_PS_CLASS_INSTR CI
Inner Join ENTITY_LOCK EL
           On CI.strm = EL.term
Inner Join PS.CS_PS_CLASS_TBL CT
           On CI.strm = CT.strm
               And nvl(CI.session_code, '<null>') = nvl(CT.SESSION_CODE, '<null>')
               and CI.class_section = CT.CLASS_SECTION
               And CI.CRSE_ID = CT.CRSE_ID
               And CI.CRSE_OFFER_NBR = CT.CRSE_OFFER_NBR
Inner Join TERMS_TO_PROCESS TTP
           On CT.institution = TTP.INSTITUTION
               And CT.strm = TTP.TERM
;
