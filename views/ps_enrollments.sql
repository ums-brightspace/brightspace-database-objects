CREATE OR REPLACE FORCE VIEW "PS_ENROLLMENTS"
AS
Select INSTITUTION
     , STRM
     , to_char(CLASS_NUMBER) as class_number
     , SESSION_CODE
     , IS_COMBINED
     , EMPLID
     , ENROLL_STATUS
     , enroll_type
From PS_ENROLL_REGULAR_STUDENT

Union All

Select INSTITUTION
     , STRM
     , CLASS_NUMBER
     , SESSION_CODE
     , IS_COMBINED
     , EMPLID
     , ENROLL_STATUS
     , enroll_type
From PS_ENROLL_COMBINED_STUDENT

Union All

Select INSTITUTION
     , STRM
     , to_char(CLASS_NUMBER) as class_number
     , SESSION_CODE
     , IS_COMBINED
     , EMPLID
     , ENROLL_STATUS
     , enroll_type
From PS_ENROLL_REGULAR_INSTRUCTOR

Union All

Select INSTITUTION
     , STRM
     , to_char(CLASS_NUMBER) as class_number
     , SESSION_CODE
     , IS_COMBINED
     , EMPLID
     , ENROLL_STATUS
     , enroll_type
From PS_ENROLL_COMBINED_INSTRUCTOR;
