Create or Replace Force View "PS_USER_NAME_CS" as
SELECT CS.emplid
     , nvl(PRF.FIRST_NAME, PRI.first_name) first_name
     , nvl(PRF.last_name, PRI.last_name) last_name
     , nvl(PRF.middle_name, PRI.MIDDLE_NAME) middle_name
From (
    Select emplid
    From PS_USER_NAME_CS_PRIMARY
    UNION
    Select emplid
    From PS_USER_NAME_CS_PREFERRED
) CS
Left Outer Join PS_USER_NAME_CS_PRIMARY PRI
                On CS.EMPLID = PRI.EMPLID
LEFT OUTER JOIN PS_USER_NAME_CS_PREFERRED PRF
                On CS.EMPLID = PRF.EMPLID;
