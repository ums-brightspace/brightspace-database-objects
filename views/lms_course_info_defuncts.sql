CREATE OR REPLACE FORCE VIEW "LMS_COURSE_INFO_DEFUNCTS" AS
Select CC.INSTITUTION
     , CC.TERM
     , CC.class_number
     , CC.is_combined
     , CC.SESSION_CODE
     , (
    Case
        When length(CC.title) > 128 Then
                trim(substr(substr(CC.title, 1, instr(CC.title, '(', -1) - 2), 1, 110)) || '... ' ||
                substr(CC.title, instr(CC.title, '(', -1))
        Else
            CC.title
        End
    ) as title
     , CC.CODE
     , CC.LMS_ID
     , '' as instructor_names
     , '' as instructor_emplids
     , '' as instructor_usernames
     , '' as parents
     , '' as master
     , '' as subject
     , '' as catalog_nbr
From COURSE_COMPLETED CC
Left Outer Join PS_COURSE_OFFERING_O CO
                On CC.INSTITUTION = CO.INSTITUTION
                    And CC.TERM = CO.TERM
                    And CC.CLASS_NUMBER = CO.CLASS_NUMBER
                    And CC.IS_COMBINED = 'n'
                    And nvl(CC.SESSION_CODE, '<null>') = nvl(CO.SESSION_CODE, '<null>')
Left Outer Join PS_COMBINED_COURSES_O PCC
                On CC.INSTITUTION = PCC.INSTITUTION
                    And CC.TERM = PCC.STRM
                    And CC.CLASS_NUMBER = PCC.SCTN_COMBINED_ID
                    And CC.IS_COMBINED = 'y'
                    And nvl(CC.SESSION_CODE, '<null>') = nvl(PCC.SESSION_CODE, '<null>')
Where CO.CLASS_NUMBER is null
  And PCC.SCTN_COMBINED_ID is null
Group By CC.INSTITUTION
       , CC.TERM
       , CC.class_number
       , CC.is_combined
       , CC.SESSION_CODE
       , CC.TITLE
       , CC.CODE
       , CC.LMS_ID

Union

Select CC.INSTITUTION
     , CC.TERM
     , CC.class_number
     , CC.is_combined
     , CC.SESSION_CODE
     , (
    Case
        When length(CC.title) > 128 Then
                trim(substr(substr(CC.title, 1, instr(CC.title, '(', -1) - 2), 1, 110)) || '... ' ||
                substr(CC.title, instr(CC.title, '(', -1))
        Else
            CC.title
        End
    ) as title
     , CC.CODE
     , CC.LMS_ID
     , '' as instructor_names
     , '' as instructor_emplids
     , '' as instructor_usernames
     , '' as parents
     , '' as master
     , '' as subject
     , '' as catalog_nbr
From COURSE_COMPLETED CC
Inner Join PS_COMBINED_CLASSES_O PCC
           On CC.IS_COMBINED = 'n'
               And CC.TERM = PCC.STRM
               And CC.CLASS_NUMBER = PCC.CLASS_NBR
               And CC.INSTITUTION = PCC.INSTITUTION
               And nvl(CC.SESSION_CODE, '<null>') = nvl(PCC.SESSION_CODE, '<null>')
Order By title;
