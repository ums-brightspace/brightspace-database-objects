create table semester_completed
(
    code        varchar2(10)  not null,
    name        varchar2(100) not null,
    lms_id      varchar2(50)  not null,
    added_on    timestamp,
    modified_on timestamp
);

create unique index semester_completed_code
    on semester_completed (code);

create unique index semester_completed_name
    on semester_completed (name);

create unique index semester_completed_lmsid
    on semester_completed (lms_id);
