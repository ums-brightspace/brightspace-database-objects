create table linked_course_parent
(
    code         varchar2(100) not null unique,
    class_number varchar2(50)  not null,
    institution  varchar2(10)  not null,
    term         varchar2(10)  not null,
    is_combined  varchar2(10),
    session_code varchar2(100)
);

create index link_crs_parent_class_nbr_idx
    on linked_course_parent (class_number);

create index link_crs_parent_inst_idx
    on linked_course_parent (institution);

create index link_crs_parent_term_idx
    on linked_course_parent (term);

create index link_crs_parent_is_comb_idx
    on linked_course_parent (is_combined);

create index link_crs_parent_sess_code_idx
    on linked_course_parent (session_code);