CREATE TABLE "USER_COMPLETED"
(
    "USERNAME"    VARCHAR2(100),
    "FIRST_NAME"  VARCHAR2(100),
    "LAST_NAME"   VARCHAR2(100),
    "EMPLID"      VARCHAR2(20),
    "EMAIL"       VARCHAR2(100),
    "CAMPUS"      VARCHAR2(100),
    "MIDDLE_NAME" VARCHAR2(100),
    added_on      timestamp,
    modified_on   timestamp,
    LMS_ID        varchar2(100)
);

create unique index USER_COMPLETED_EMPLID_uindex
    on USER_COMPLETED (EMPLID);

create index USER_COMPLETED_LMS_ID_IDX
    on USER_COMPLETED (LMS_ID);

alter table USER_COMPLETED
    add constraint user_compled_lms_id unique (LMS_ID);