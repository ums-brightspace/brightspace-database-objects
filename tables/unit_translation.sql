create table unit_translation
(
    id          NUMBER,
    institution varchar2(10) not null,
    acad_group varchar2(100),
    subject varchar2(100),
    acad_org varchar2(100),
    unit varchar2(100) not null
);

/*create sequence for auto-increment column*/
CREATE SEQUENCE unit_translation_seq START WITH 1;

/*create trigger for auto-increment column*/
CREATE OR REPLACE TRIGGER unit_translation_trig
    BEFORE INSERT ON unit_translation
    FOR EACH ROW

BEGIN
    SELECT unit_translation_seq.NEXTVAL
    INTO   :new.id
    FROM   dual;
END;
