create table admin_page_permission
(
    id                  int generated as identity,
    admin_page_id       int not null
        constraint ADMIN_PAGE_PERM_PAGE_ID_FK
            references ADMIN_PAGE(id),
    admin_permission_id int not null
        constraint admin_page_perm_perm_id_fk
            references ADMIN_PERMISSION(id),
    CONSTRAINT admin_page_perm_pk PRIMARY KEY (id)
);
