create table linked_course_child
(
    child_code  varchar2(100) not null,
    code         varchar2(100) not null,
    class_number varchar2(50)  not null,
    institution  varchar2(10)  not null,
    term         varchar2(10)  not null,
    is_combined  varchar2(10),
    session_code varchar2(100),

    CONSTRAINT fk_lcc_child_code
        FOREIGN KEY (child_code)
            REFERENCES LINKED_COURSE_child (code)
);

create index link_crs_child_class_nbr_idx
    on linked_course_child (class_number);

create index link_crs_child_inst_idx
    on linked_course_child (institution);

create index link_crs_child_term_idx
    on linked_course_child (term);

create index link_crs_child_is_comb_idx
    on linked_course_child (is_combined);

create index link_crs_child_sess_code_idx
    on linked_course_child (session_code);
