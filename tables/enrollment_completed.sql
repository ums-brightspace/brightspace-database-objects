create table enrollment_completed
(
    user_lms_id   varchar2(100) not null,
    course_lms_id varchar2(100)  not null,
    "TYPE"        varchar2(5)  not null,
    ENROLL_STATUS varchar2(5)  not null,
    added_on      timestamp,
    modified_on   timestamp
);

create index enroll_complete_user_id_IDX
    on enrollment_completed (user_lms_id);

create index enroll_complete_course_id_IDX
    on enrollment_completed (course_lms_id);
