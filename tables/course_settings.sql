create table COURSE_SETTINGS
(
    INSTITUTION varchar2(6) not null,
    IS_ACTIVE number(1,0) default 0 not null,
    SET_START_DATE number(1,0) default 0 not null,
    START_DATE_DAYS_OFFSET number(3,0) default 0 not null,
    SET_END_DATE number(1,0) default 0 not null,
    END_DATE_DAYS_OFFSET number(3,0) default 0 not null
);

create unique index COURSE_SETTINGS_INSTITUTION_U
    on COURSE_SETTINGS (INSTITUTION);

