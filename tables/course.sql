create table course
  (
      code         varchar2(100)  not null,
      title        varchar2(1000) not null,
      parents      varchar2(1000) not null,
      master       varchar2(100)  not null,
      semester     varchar2(100)  not null,
      class_number varchar2(50)   not null,
      institution  varchar2(10)   not null,
      term         varchar2(10)   not null,
      is_combined  varchar2(10),
      session_code varchar2(100)
  );