create table log
(
    id          int generated as identity,
    date_time   timestamp     not null,
    entity      varchar2(50)  not null,
    ums_id      varchar2(1000),
    lms_id      varchar2(100),
    action      varchar2(50)  not null,
    status_code varchar2(10),
    message     varchar2(100) not null,
    data        varchar2(4000),
    constraint LOG_PK
        primary key (id)
);
