create table ADMIN_USER_PERMISSION
(
    id int generated as identity,
    admin_user_id       int not null
        constraint ADMIN_USER_PERM_USER_ID_FK
            references ADMIN_USER(id),
    admin_permission_id int not null
        constraint ADMIN_USER_PERM_PERM_ID_FK
            references ADMIN_PERMISSION(id),
    CONSTRAINT admin_user_permission_pk PRIMARY KEY (id)
);
