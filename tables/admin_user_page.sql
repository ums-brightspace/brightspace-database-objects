create table ADMIN_USER_PAGE
(
    id int generated as identity,
    admin_user_id       int not null
        constraint ADMIN_USER_PAGE_USER_ID_FK
            references ADMIN_USER(ID),
    admin_page_id number not null
        constraint ADMIN_USER_PAGE_PAGE_ID_FK
            references ADMIN_PAGE(id),
    CONSTRAINT admin_user_page_pk PRIMARY KEY (id)
);
