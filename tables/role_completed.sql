create table role_completed
(
    user_lms_id   varchar2(100) unique not null
    constraint role_completed_user_lms_id_fk
        references USER_COMPLETED(lms_id),
    role_ind      varchar2(5)  not null,
    added_on      timestamp,
    modified_on   timestamp
);
